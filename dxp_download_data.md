Here, you will find an overview of the open source information of this product. The detailed information can be found within the repository at [GitLab](https://gitlab.com/ourplant.net/products/s3-0011-waffle-pack-support-41).

| document                 | download options |
| :----------------------- | ---------------: |
| operating manual         | [de](https://gitlab.com/ourplant.net/products/s3-0011-waffle-pack-support-41/-/raw/main/01_operating_manual/S3-0011_B_BA_Waffle%20Pack%20Support.pdf), [en](https://gitlab.com/ourplant.net/products/s3-0011-waffle-pack-support-41/-/raw/main/01_operating_manual/S3-0011_B_OM_Waffle%20Pack%20Support.pdf)                 |
| assembly drawing         |  [de](https://gitlab.com/ourplant.net/products/s3-0011-waffle-pack-support-41/-/raw/main/02_assembly_drawing/s3-0011_A_ZNB_Waffle_Pack_Support_41.PDF)                 |
| circuit diagram          |                  |
| maintenance instructions |[de](https://gitlab.com/ourplant.net/products/s3-0011-waffle-pack-support-41/-/raw/main/04_maintenance_instructions/S3-0011_B_WA_WPS.pdf), [en](https://gitlab.com/ourplant.net/products/s3-0011-waffle-pack-support-41/-/raw/main/04_maintenance_instructions/S3-0011_B_MI_WPS.pdf)                  |
| spare parts              |                  |

